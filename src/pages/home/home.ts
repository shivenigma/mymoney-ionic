import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {DatabaseProvider} from "../../providers/database/database";
import {Subscription} from "rxjs/Subscription";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy {
  public expenseForm: FormGroup;
  public entries;
  private dbReady: boolean;
  private dbSubscription: Subscription;
  private querySubscription: Subscription;

  constructor(public navCtrl: NavController,
              private formBuilder: FormBuilder,
              private database: DatabaseProvider) {
    this.expenseForm = formBuilder.group({
      'date': new FormControl(new Date().toISOString(), [Validators.required]),
      'item': new FormControl('', [Validators.required]),
      'amount': new FormControl(null, [Validators.required]),
      'type': new FormControl("0", [Validators.required])
    });
  }

  save(formData) {
    this.database.addItem(formData).then(success => {
      this.expenseForm.reset({'date': new Date().toISOString(), 'type': 0});
    });
  }

  getEntries() {
    this.querySubscription = this.database.getSummary().subscribe(success => {
      console.log('data');
      console.log(success);
    }, error => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.dbSubscription = this.database.getDatabaseStatus().subscribe((res) => {
      this.dbReady = res;
      if (this.dbReady) {
        this.getEntries();
      }
    })
  }

  ngOnDestroy() {
    this.dbSubscription.unsubscribe();
    this.querySubscription.unsubscribe();
  }

}
