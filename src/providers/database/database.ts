import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Platform} from "ionic-angular";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/fromPromise';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  public database: SQLiteObject;
  public databaseReady: BehaviorSubject<boolean>;

  constructor(private sqlite: SQLite, private platform: Platform) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'mymoney.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.databaseReady.next(true);
          this.database = db;
          db.executeSql('CREATE TABLE IF NOT EXISTS expense(id INTEGER PRIMARY KEY AUTOINCREMENT, item TEXT, amount REAL, type INTEGER, date TEXT)', {})
            .then(res => {
            }, (error) => {
              console.log(error);
            })
        }, error => {
          console.log(error);
        })
    });
  }

  addItem(data: ExpenseModel): Promise<any> {

    return this.database.executeSql('INSERT INTO expense VALUES (NULL, ?, ?, ?, ?)', [data.item, data.amount, data.type, data.date]).then(success => {
      return success;
    }, error => {
      return error;
    });
  }

	getItems(month: string): Observable<any> {
		console.log(month);
    return Observable.create(observer => {
      this.platform.ready().then(() => {
        this.database.executeSql("SELECT * FROM expense WHERE strftime('%m', date) = ?", [month])
          .then(success => {
            observer.next(this.getValuesFromDbResult(success));
            observer.complete();
          }, error => {
            console.log(error);
            observer.complete();
          })
      })
    });
  }

  getSummary(): Observable<any> {
    return Observable.create(observer => {
      this.platform.ready().then(() => {
        this.database.executeSql("SELECT SUM(amount), type from expense WHERE strftime('%m',date) = strftime('%m', date('now')) GROUP BY type", [])
          .then(success => {
            observer.next(this.getValuesFromDbResult(success));
            observer.complete();
          }, error => {
            console.log(error);
            observer.complete();
          })
      });
    });
  }

  getDatabaseStatus(): Observable<any> {
    return this.databaseReady.asObservable();
  }

  private getValuesFromDbResult(success) {
    const expenses = [];
    for (let i = 0; i < success.rows.length; i++) {
      expenses.push(success.rows.item(i));
    }
    return expenses;
  }
}
