interface ExpenseModel {
  id? : number,
  item: string,
  amount: number,
  type: number,
  date: string,
}
