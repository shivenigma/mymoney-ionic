import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {DatabaseProvider} from "../../providers/database/database";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage implements OnInit, OnDestroy {
	expenses = [];
	querySubscription: Subscription;
	month = new Date();
    isoDate: string;
	ngOnInit() {
		if(this.navParams.get('month')) {
			this.month = new Date(this.month.getFullYear(), this.navParams.get('month'));
		}
        this.isoDate = this.month.toISOString();
        this.populateData();
      
    }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
 	      private database: DatabaseProvider) {
  }
  getPaddedMonth(date: Date) :string {
  	return ('0' + (date.getMonth() + 1)).slice(-2);
  }
  
        dateChanged(event) {
                this.month = new Date(this.isoDate);
                this.expenses = [];
                this.populateData();
        }
        populateData() {
            this.querySubscription = this.database.getItems(this.getPaddedMonth(this.month))
                    .subscribe(success => {
                            console.log(success);
      success.map((item) => {
        const day = new Date(item.date).getDate();
        if(!this.expenses[day]) {
          this.expenses[day] = [];
        }
        this.expenses[day].push(item);
      });}, error => {
    });
        }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
  }
}
